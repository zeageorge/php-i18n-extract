# PHP i18n Extract

Simple i18n messages extractor

## Install

Via Composer

``` bash
$ composer require zeageorge/php-i18n-extract
```

## Usage

``` php
<?php

declare(strict_types=1);

namespace zeageorge\i18n_extract\example;

use zeageorge\i18n_extract\Extractor;

require 'vendor/autoload.php';

$extractor = new Extractor(__DIR__ . '/i18nExtract.config.php');

$extractor->extract();

```

