

const CODE_TOO_MANY_OTP_REQUESTS_ERROR_MESSAGE = /*locale*/'OTP requests limit reached, try again after {minutes} minutes';

message: $this->t('No account was found for this email address'))]))->send();

$data = $this->t('The captcha key must be {len} characters long', ['len' => 5]);

$this->t('The new password must be between {length_min} and {length_max} characters long and it must contain at least three of the four available character types: lowercase letters {low}, uppercase letters {upp}, numbers {num}, and symbols {symbols}', ['length_min' => $min_len, 'length_max' => $max_len, 'low' => 'a-z', 'upp' => 'A-Z', 'num' => '0-9', 'symbols' => '~!@#$%^&*()_+{}|:"<>?`-=[]\;\',./']));

