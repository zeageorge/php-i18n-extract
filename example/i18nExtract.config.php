<?php

declare(strict_types=1);

namespace zeageorge\i18n_extract\example;

use function file_get_contents;

return [
  //////// files finder settings:
  'findFilesMethod' => [$this, 'scanPath'], // call this method to find the files
  // Start search from this folder (without trailing slash)
  // we can also use a filename (absolute path), eg. /var/tmp/lala.php, in this case
  // only the file will be searched for messages
  'sourcePath' => __DIR__,
  // use ['.*'] to include all the files
  'fileExtensionsToSearch' => ['.php', '.twig'],
  // Exclude the following files/folders (relative to sourcePath, without trailing slash).
  // Do not use wildcard character (*)
  'exclude' => [
    '/locale',
    '/i18nExtract.config.php',
    /*, '/logs/log1.txt'*/
  ],

  //////// filter messages settings:
  'filterMessagesMethod' => [$this, 'processFiles'], // call this method to filter every message
  'regex' => [ // use the following patterns to find messages within the files
    "/->t\(['\"](.*)['\"]\)/",
    "/->t\(['\"](.*)['\"], \[['\"]/",
    "/->t\(['\"](.*)['\"],\[['\"]/",
    "/\{\{t\(['\"](.*)['\"]\)/",
    "/\{\{ t\(['\"](.*)['\"]\)/",
    "/\{\{ t\(['\"](.*)['\"],\{/",
    "/\{\{ t\(['\"](.*)['\"], \{/",
    "/\{\{t\(['\"](.*)['\"], \{/",
    "/\{\{t\(['\"](.*)['\"],\{/",
    "/\/\*locale\*\/[ '\"](.*)['\"]/",
  ],
  'trim' => [ // trim the following strings from each message
    'fromStart' => ['"', "'"],
    'fromEnd' => ["'", "',", "', ", "')", '"', '",', '", ', '")'],
  ],

  //////// format messages settings:
  'messagesFormaterMethod' => [$this, 'formatMessages'], // call this method to format the messages and prepare them for the template

  //////// output messages settings:
  'outputFilePath' => __DIR__ . '/locale/messages.en-US.php', // where messages.en-US.php will be saved
    // the template to use
  'template' => file_get_contents(__DIR__ . '/template.php'), 
];
