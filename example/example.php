<?php

declare(strict_types=1);

namespace zeageorge\i18n_extract\example;

use zeageorge\i18n_extract\Extractor;

require 'vendor/autoload.php';

$extractor = new Extractor(__DIR__ . '/i18nExtract.config.php');

$extractor->extract();

