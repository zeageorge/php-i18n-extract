<?php

declare(strict_types=1);

namespace zeageorge\i18n_extract;

use Exception;

/**
 * Description of FileWriteFailedException
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class FileWriteFailedException extends Exception {
}