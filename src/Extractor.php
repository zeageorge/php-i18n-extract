<?php

declare(strict_types=1);

namespace zeageorge\i18n_extract;

use InvalidArgumentException;

use function
  mkdir,
  ksort,
  is_dir,
  is_file,
  scandir,
  natsort,
  dirname,
  in_array,
  is_array,
  mb_substr,
  mb_strlen,
  array_push,
  file_exists,
  is_readable,
  is_callable,
  str_ends_with,
  preg_match_all,
  call_user_func,
  str_starts_with,
  file_get_contents;

use const 
  SORT_STRING,
  PREG_SET_ORDER,
  DIRECTORY_SEPARATOR;

/**
 * Description of Extractor
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Extractor {
  protected object $config;

  /**
   * Constructor
   *
   * @param string $configFilePath
   */
  public function __construct(protected string $configFilePath) {
    $this->config = (object) require $configFilePath;
  }

  /**
   * 
   * @param array $params configuration parameters overrides
   * @throws InvalidArgumentException
   * @throws FileNotReadableException
   * @throws InvalidRegexException
   * @throws FileWriteFailedException
   */
  public function extract(array $params = []) {
    $p = (object) ($params + (array) $this->config);

    $this->validateConfiguration($p);

    /** @var string[] file paths */
    $filesToProcess = is_file($p->sourcePath) ? [$p->sourcePath] : 
      call_user_func($p->findFilesMethod, $p->sourcePath, $p->fileExtensionsToSearch, 
        array_map(fn (string $excl): string => str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $p->sourcePath . $excl), $p->exclude));

    /** @var array [string => array]*/
    $filesMessages = call_user_func($p->filterMessagesMethod, $filesToProcess, $p->regex, $p->trim);

    $finalMessagesArray = [];

    foreach ($filesMessages as $filename => $messages) {
      foreach ($messages as $message) {
        $finalMessagesArray[$message] = $message;
      }
    }
    
    ksort($finalMessagesArray, SORT_STRING);
    
    /** @var string */
    $formatedMessages = call_user_func($p->messagesFormaterMethod, $finalMessagesArray);

    /** @var string */
    $dirName = dirname($p->outputFilePath);

    !file_exists($dirName) && mkdir($dirName, 0777, true);

    file_put_contents($p->outputFilePath, str_replace('{messages}', $formatedMessages, $p->template)) === false && 
      throw new FileWriteFailedException("File {$p->outputFilePath} couldn't be created");
  }

  /**
   * 
   * @param object $p
   * @throws InvalidArgumentException
   */
  protected function validateConfiguration(object $p) {
    !is_readable($p->sourcePath) && throw new InvalidArgumentException("[sourcePath] must be readable");
    
    !is_array($p->fileExtensionsToSearch) && throw new InvalidArgumentException("[fileExtensionsToSearch] must be an array");
    !is_array($p->exclude) && throw new InvalidArgumentException("[exclude] must be an array");
    !is_array($p->regex) && throw new InvalidArgumentException("[regex] must be an array");
    !is_array($p->trim) && throw new InvalidArgumentException("[trim] must be an array");

    !is_callable($p->findFilesMethod, true) && throw new InvalidArgumentException("[findFilesMethod] must be a callable");
    !is_callable($p->filterMessagesMethod, true) && throw new InvalidArgumentException("[filterMessagesMethod] must be a callable");
    !is_callable($p->messagesFormaterMethod, true) && throw new InvalidArgumentException("[messagesFormaterMethod] must be a callable");

    !isset($p->template) && throw new InvalidArgumentException("[template] must be a string");
  }

  /**
   *
   * @param string $path - directory path
   * @param string[] $exts
   * @param string[] $exclude
   * @return string[]
   */
  public function scanPath(string $path, array $exts = ['.*'], array $exclude = []): array {
    /** @var string[] */
    $files = [];

    if (is_file($path)) {
      if (is_readable($path)) {
        foreach ($exts as $ext) {
          if ($ext == '.*' || mb_substr($path, mb_strlen($path) - mb_strlen($ext)) == $ext) {
            $files[] = $path;
            return $files;
          }  
        }
      }
    } elseif (is_dir($path)) {
      foreach (scandir($path) as $object) {
        if ($object != "." && $object != "..") {
          $fullPath = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path . DIRECTORY_SEPARATOR . $object);
          !in_array($fullPath, $exclude) && array_push($files, ...$this->scanPath($fullPath, $exts, $exclude));
        }
      }
    }

    return $files;
  }

  /**
   * @param array $filesToProcess
   * @param array $methodNamesRegex
   * @param array $trim
   * @return array
   * @throws FileNotReadableException if a file is not accessible
   * @throws InvalidRegexException if a regex is invalid
   */
  public function processFiles(array $filesToProcess, array $methodNamesRegex, array $trim): array {
    /** @var string[] */
    $filesMessages = [];

    foreach ($filesToProcess as $filePath) {
      /** @var string|false */
      $fileContent = file_get_contents($filePath);
    
      $fileContent === false && throw new FileNotReadableException("Error: Couldn't read file {$filePath}");
    
      $realFilePath = realpath($filePath);
      $filesMessages[$realFilePath] = [];
    
      foreach ($methodNamesRegex as $idx => $pattern) {
        $result = preg_match_all($pattern, $fileContent, $matches, PREG_SET_ORDER);

        $result === false && throw new InvalidRegexException("Error: Regex pattern {$pattern} on index {$idx} is not valid");
    
        if ($result) {
          foreach ($matches as $match) {
            if (isset($match[1])) $filesMessages[$realFilePath][] = $this->trim($match[1], $trim);
          }
        }
      }

      natsort($filesMessages[$realFilePath]);
    }
    
    ksort($filesMessages, SORT_STRING);

    return $filesMessages;
  }

  /**
   * 
   * @param array $messages
   * @return string
   */
  public function formatMessages(array $messages): string {
    /** @var string */
    $finalMessages = '';
    
    foreach ($messages as $key => $value) {
      $finalMessages .= "\n  '{$key}' => \n    '{$value}',";
    }
    
    return $finalMessages;
  }

  /**
   * 
   * @param string $message
   * @param array $trims
   * @return string
   */
  protected function trim(string $message, array $trims): string {
    return $this->trimFromEnd($this->trimFromStart($message, $trims['fromStart']), $trims['fromEnd']);
  }

  /**
   * 
   * @param string $message
   * @param array $trims
   * @return string
   */
  protected function trimFromStart(string $message, array $trims): string {
    $msg = $message;

    foreach ($trims as $str) {
      if (str_starts_with($msg, $str)) {
        $msg = mb_substr($msg, mb_strlen($str));
      }
    }

    return $msg;
  }

  /**
   * 
   * @param string $message
   * @param array $trims
   * @return string
   */
  protected function trimFromEnd(string $message, array $trims): string {
    $msg = $message;

    foreach ($trims as $str) {
      if (str_ends_with($msg, $str)) {
        $messageLen = mb_strlen($msg);
        $strLen = mb_strlen($str);
        $msg = mb_substr($msg, 0, $messageLen - $strLen);
      }
    }

    return $msg;
  }
}