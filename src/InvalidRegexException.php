<?php

declare(strict_types=1);

namespace zeageorge\i18n_extract;

use Exception;

/**
 * Description of InvalidRegexException
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class InvalidRegexException extends Exception {
}