<?php

declare(strict_types=1);

namespace zeageorge\i18n_extract;

use Exception;

/**
 * Description of FileNotReadableException
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class FileNotReadableException extends Exception {
}